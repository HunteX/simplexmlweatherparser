﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace SimpleXmlWeatherParser
{
    class Program
    {
        // API Docs https://developer.yahoo.com/weather/

        static string BuildXmlSource(string city)
        {
            string query = string.Format("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"{0}\")", city);

            string xmlSource = 
                string.Format(
                    "https://query.yahooapis.com/v1/public/yql?q={0}&format=xml&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys",
                    HttpUtility.UrlEncode(query)
                    );

            return xmlSource;
        }

        static void Main(string[] args)
        {
            var xmlDoc = new XmlDocument();

            string xmlDataSource = BuildXmlSource("Ulyanovsk");

            // загружаем данные из сети
            xmlDoc.Load(xmlDataSource);

            // настраиваем namespace для XPath-запроса
            XmlNamespaceManager ns = new XmlNamespaceManager(xmlDoc.NameTable);
            ns.AddNamespace("yweather", "http://xml.weather.yahoo.com/ns/rss/1.0");

            // получаем ноды
            XmlNodeList nodes = xmlDoc.SelectNodes("/query/results/channel/item/yweather:forecast", ns);

            Console.WriteLine("Дата\t\tМинимум\tМаксимум\tОписание");
            Console.WriteLine("---------------------------------------------------");

            foreach (XmlNode node in nodes)
            {
                Console.WriteLine("{0}\t{1:00.0}\t{2:00.0}\t\t{3}", 
                    node.Attributes["date"].Value,
                    ToCelcius(double.Parse(node.Attributes["low"].Value)),
                    ToCelcius(double.Parse(node.Attributes["high"].Value)),
                    node.Attributes["text"].Value
                );
            }

            Console.WriteLine("\nНажмите любую клавишу для завершения программы ...");
            Console.ReadKey();
        }

        static double ToCelcius(double f)
        {
            return 5.0 / 9.0 * (f - 32);
        }
    }
}
